Star = require '../lib/star'
mdb = new Star
  movies: [
    {movieId: 1, movieName: 'Fight Club', budget: 10}
    {movieId: 2, movieName: 'Inglourious Basterds', budget: 20}
    {movieId: 3, movieName: 'The Godfather', budget: 30}
  ],
  actors: [
    {actorId: 1, actorName: 'Brad Pitt'}
    {actorId: 2, actorName: 'Edward Norton'}
    {actorId: 3, actorName: 'Diane Kruger'}
    {actorId: 4, actorName: 'Marlon Brando'}
    {actorId: 5, actorName: 'Al Pacino'}
    {actorId: 6, actorName: 'James Caan'}
  ],
  actors_movies: [
    {actorId: 1, movieId: 1}
    {actorId: 2, movieId: 1}
    {actorId: 1, movieId: 2}
    {actorId: 3, movieId: 2}
    {actorId: 4, movieId: 3}
    {actorId: 5, movieId: 3}
    {actorId: 6, movieId: 3}

  ],
  directors: [
    {directorId: 1, directorName: 'David Fincher'}
    {directorId: 2, directorName: 'Quentin Tarantino'}
    {directorId: 3, directorName: 'Eli Roth'}
    {directorId: 4, directorName: 'Francis Ford Coppola'}
  ],
  directors_movies: [
    {directorId: 1, movieId: 1}
    {directorId: 2, movieId: 2}
    {directorId: 3, movieId: 2}
    {directorId: 4, movieId: 3}
  ]


describe "apphire-BI", -> 
  describe "core", ->
    it "Aggergate", ->
      log mdb.aggregate Star.aggrTypes.SUM, 'budget', ['movieName'],
        actorName: {'Brad Pitt': 1}
        directorName: {'David Fincher': 1}
      log mdb.aggregate Star.aggrTypes.COUNT, 'actorId', ['actorName'],
        directorName: {'David Fincher': 1}
      


