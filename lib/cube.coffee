module.exports = class Cube
	@aggrTypes:
		COUNT: 'count'
		SUM: 'sum'
		AVG: 'avg'
		MAX: 'max'
		MIN: 'min'
	constructor: (data, @name) ->
		@schema = data[0] #TODO - parse first row and determine field type
		@data = data

	passedFilter = (row, where)->
		for field, filter of where
			if not filter[row[field]]? then return false
		return true

	aggregate: (how, what, byWhat, where)->
		result = if byWhat? then {} else 0
		reducer = switch how
			when Cube.aggrTypes.COUNT then (a,b)-> a + 1
			when Cube.aggrTypes.SUM then (a,b)-> a + b
			when Cube.aggrTypes.AVG then (a,b)-> a + b
			when Cube.aggrTypes.MAX then (a,b)-> Math.max(a, b)
			when Cube.aggrTypes.MIN then (a,b)-> Math.min(a, b)

		for row in @data
			if where?
				if not passedFilter(row, where) then continue
			if byWhat?
				curNode = result
				for grouping, index in byWhat
					if index isnt byWhat.length-1 #not the last element
						curNode = curNode[row[grouping]] ?= {}
					else
						curNode[row[grouping]] = reducer(curNode[row[grouping]] or 0, row[what])
			else
				result = reducer(result, row[what])
		return result
