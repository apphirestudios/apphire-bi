Cube = require './cube'
module.exports = class Star
	@aggrTypes: Cube.aggrTypes
	cubes: {}

	mergeFilters = (filterSets...)->
		result = {}
		for filterSet in filterSets
			for fieldName, filter of filterSet
				result[fieldName] ?= {}
				for filterName, filterValue of filter
					addFilterValue = true
					
					for innerFilterSet in filterSets when innerFilterSet isnt filterSet
						if innerFilterSet[fieldName]?					
							if not innerFilterSet[fieldName]?[filterName]?
								addFilterValue = false
								break

					if addFilterValue then result[fieldName][filterName] = 1
		
		return result

	createCubeFilter = (cube, where)->
		filter = {}
		for fieldName of cube.schema when where[fieldName]?
			filter[fieldName] = {}
			for filterValue of where[fieldName]
				filter[fieldName][filterValue] = 1
		return filter

	getCrossFilters: (targetCube, parentCube, parentField, globalWhere)->
		filtersMe = createCubeFilter targetCube, globalWhere
		for fieldName of targetCube.schema when fieldName isnt parentField
			for associatedCube in @getCubeByFieldName fieldName when associatedCube.name isnt targetCube.name and associatedCube.name isnt parentCube.name
				filtersMe = mergeFilters filtersMe, @getCrossFilters(associatedCube, targetCube, fieldName, globalWhere)


		filtered = {}
		for targetField of targetCube.schema when parentCube.schema[targetField]?
			filtered[targetField] = targetCube.aggregate Star.aggrTypes.COUNT, '*', [targetField], filtersMe
		return filtered

	getCubeByFieldName: (fieldName)->
		result = []
		for cubeName, cube of @cubes
			if cube.schema[fieldName]? then result.push cube
		return result
	
	getCubeFilters: (targetCube, globalWhere)->
		filtersMe = createCubeFilter targetCube, globalWhere
		for fieldName of targetCube.schema
			for associatedCube in @getCubeByFieldName fieldName when associatedCube.name isnt targetCube.name
				filtersMe = mergeFilters filtersMe, @getCrossFilters(associatedCube, targetCube, fieldName, globalWhere)
		return filtersMe

	constructor: (data) ->
		for tableName, tableData of data
			@cubes[tableName] = new Cube tableData, tableName
	###
		======= Main aggregate function =======
		@how: aggregation type enum (SUM, COUNT, etc), see Cube.aggrTypes
		@what: aggregation measure, could be from any table
		@byWhat: array of aggregation groupings
		@where: hash of where clauses in {fieldName: {f1:null, f2:null, f3:null}}
		======================================
	###
	aggregate: (how, what, byWhat, where)->		
		cubes = @getCubeByFieldName(what)
		targetCube = cubes[0]
		log @getCubeFilters(targetCube, where or {})
		return targetCube.aggregate how, what, byWhat, @getCubeFilters(targetCube, where or {})



	
